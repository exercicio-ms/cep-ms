package com.fercugliandro.mastertech.cepms.client;

import com.fercugliandro.mastertech.cepms.model.Cep;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cep", url = "http://viacep.com.br/")
public interface ViaCepClient {

    @GetMapping("/ws/{cep}/json/")
    Cep consultarCep(@PathVariable String cep);

}
