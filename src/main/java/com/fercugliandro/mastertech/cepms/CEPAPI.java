package com.fercugliandro.mastertech.cepms;

import com.fercugliandro.mastertech.cepms.model.Cep;
import com.fercugliandro.mastertech.cepms.service.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CEPAPI {

    @Autowired
    private CepService service;

    @GetMapping("/cep/{cep}")
    public ResponseEntity<?> obterCep(@PathVariable String cep) {
        Cep cepRetorno = service.consultarCep(cep);

        return new ResponseEntity<>(cepRetorno, HttpStatus.OK);
    }

}
