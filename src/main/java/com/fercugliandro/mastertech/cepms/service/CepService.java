package com.fercugliandro.mastertech.cepms.service;

import com.fercugliandro.mastertech.cepms.model.Cep;

public interface CepService {

    Cep consultarCep(String cep);
}
