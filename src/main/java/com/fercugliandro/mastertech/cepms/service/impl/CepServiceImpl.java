package com.fercugliandro.mastertech.cepms.service.impl;

import com.fercugliandro.mastertech.cepms.client.ViaCepClient;
import com.fercugliandro.mastertech.cepms.model.Cep;
import com.fercugliandro.mastertech.cepms.service.CepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.cloud.sleuth.annotation.SpanTag;
import org.springframework.stereotype.Service;

@Service
public class CepServiceImpl implements CepService {

    @Autowired
    private ViaCepClient cepClient;

    @Override
    @NewSpan
    public Cep consultarCep(@SpanTag("cep") String cep) {

        Cep retorno = cepClient.consultarCep(cep);
        if (retorno == null) {
            return null;
        }

        return retorno;
    }
}
